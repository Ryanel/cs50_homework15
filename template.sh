# template.sh - edit this file
function usage
{
  echo "usage: $0 arguments ..."
  if [ $# -eq 1 ]
  then echo "ERROR: $1"
  fi
}

# Your script starts after this line.

if [[ $# -eq 0 ]] ; then
  usage
  exit 0
fi

echo "Corwin McKnight"
date

echo ""


for arg
do
  case $arg in
    TestError)
      usage "TestError found"
      ;;
    now)
      currentTime=$(date +"%I:%M:%S %p")
      echo "It is now: $currentTime"
      ;;
    *)
      usage "Don't know what to do with $arg"
      ;;
  esac
  echo "*****"
done
